package proyectoarquitectura;
public class Cliente {    
    private int codigoCliente;
    private String razonSocial;
    
    public Cliente() { }
    
    public Cliente(int cod, String raz)
    { 
        codigoCliente= cod;
        razonSocial = raz;
    }

    /**
     * @return the codigoCliente
     */
    public int getCodigoCliente() {
        return codigoCliente;
    }

    /**
     * @param codigoCliente the codigoCliente to set
     */
    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    
}


