/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoarquitectura;

public class ListaPedidos {
    
    private NodoPedido head = null;
    private NodoPedido actual = null; // esta variable es utilizada como el variable auxiliar interna de la lista.
    
    public void agregarPedido(Pedido pedido) {
        this.actual = head;
        if( this.head == null ){
            // primer nodo de la lista
            this.head = new NodoPedido(pedido);
            this.actual = this.head;
        } else {
            // la lista ya tiene nodos
            while( this.actual.getNext() != null ){
                this.actual = this.actual.getNext();
            }
            this.actual.setNext( new NodoPedido(pedido) );
            this.actual = this.actual.getNext();
        }
    }
    
    public void agregarPedido( Pedido pedido, int posicion ) {
        if( posicion < 0 ) {
            return;
        }
        this.actual = head;
        if( this.head == null ){
            // primer nodo de la lista
            this.head = new NodoPedido(pedido);
            this.actual = this.head;
        } else if( posicion == 0 ){
            // lista no vacia y se pide ponerlo en la cabecera
            this.head = new NodoPedido(pedido);
            this.head.setNext( this.actual );
        } else {
            // la lista ya tiene nodos
            int cont = 1;
            while( this.actual.getNext() != null ){
                if( cont++ == posicion ) {
                    break;
                }
                this.actual = this.actual.getNext();
            }
            NodoPedido aux = this.actual.getNext();
            this.actual.setNext( new NodoPedido(pedido) );
            this.actual = this.actual.getNext();
            this.actual.setNext(aux);
        }
    }
    
    public boolean eliminar(Pedido pedido) { 
        this.actual = this.head;
        if( this.actual == null ) {
            return false;
        }
        while( this.actual.getNext() != null ){
            if( this.actual.getNext().getDato().equals(pedido) ){
                // si es el dato pedido
                if( this.actual.getNext() == this.head )
                    // podria ser el unico nodo de la lista
                    this.head = null;
                this.actual.setNext( this.actual.getNext().getNext() );
                return true;
            }
            this.actual = this.actual.getNext();
        }
        return false;
    }
    
    public int tamanio() {
        int i = 0;
        NodoPedido aux = head;
        while( aux != null ){
            aux = aux.getNext();
            i++;
        }
        return i;
    }
    
    public Pedido obtenerPedido(int posicion)
    {
        if( posicion < 0 ){
            return null;
        }
        
        NodoPedido aux = head;
        int contador = 0;
        
        while( aux != null ) {
            if( contador == posicion ) {
                //  llegamos al nodo
                this.actual = aux;
                return aux.getDato();
            }
            aux = aux.getNext();
            contador++;
        }
        
        return null;
    }
    
}
