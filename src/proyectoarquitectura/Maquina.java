package proyectoarquitectura;

public class Maquina  extends LectorDeExcel// en el fondo es una clase que crea listas de pedidos
{ 
    private String codMaquina;
    private double estimacionTiempoElaboracion;
    private ListaPedidos pedidosSecuenciados;
    
    public Maquina(String codMaquina){
        this.codMaquina = codMaquina;
        this.listaPedidos = new ListaPedidos();
        this.pedidosSecuenciados = new ListaPedidos();
        this.leerExcel(codMaquina);
    }
    
    public String getCodMaquina(){
        return this.codMaquina;
    }

    /**
     * @param codMaquina the codMaquina to set
     */
    public void setCodMaquina(String codMaquina) {
        this.codMaquina = codMaquina;
    }

    /**
     * @return the listaPedidos
     */
    public ListaPedidos getListaPedidos() {
        return listaPedidos;
    }

    /**
     * @param listaPedidos the listaPedidos to set
     */
    public void setListaPedidos(ListaPedidos listaPedidos) {
        this.listaPedidos = listaPedidos;
    }
    
    public void agregarPedido( Pedido pedido ) {
        this.listaPedidos.agregarPedido(pedido);
    }

    /**
     * @return the estimacionTiempoElaboracion
     */
    public double getEstimacionTiempoElaboracion() {
        return estimacionTiempoElaboracion;
    }

    /**
     * @param estimacionTiempoElaboracion the estimacionTiempoElaboracion to set
     */
    public void setEstimacionTiempoElaboracion(double estimacionTiempoElaboracion) {
        this.estimacionTiempoElaboracion = estimacionTiempoElaboracion;
    }

    /**
     * @return the pedidosSecuenciados
     */
    public ListaPedidos getPedidosSecuenciados() {
        return pedidosSecuenciados;
    }

    /**
     * @param pedidosSecuenciados the pedidosSecuenciados to set
     */
    public void setPedidosSecuenciados(ListaPedidos pedidosSecuenciados) {
        this.pedidosSecuenciados = pedidosSecuenciados;
    }
    
    
        
}
   

  

