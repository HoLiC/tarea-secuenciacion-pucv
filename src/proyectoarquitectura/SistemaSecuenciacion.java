
package proyectoarquitectura;

public class SistemaSecuenciacion
{
    public Maquina [] maquinas;
    int mesParaCalculo;
    int añoParaCalculo;
    
    public SistemaSecuenciacion(int m, int a)
    {
        mesParaCalculo = m;
        añoParaCalculo = a;
        maquinas = new Maquina[3];
        maquinas[0] = new Maquina("M1");
        maquinas[1] = new Maquina("M2");
        maquinas[2] = new Maquina("M3");
    }
    
    public double[] promediosMaquinas(){
        double[] promedios = new double[3];
        return promedios;
    }
    
    public void calcularTiempoFabricacion(int mes, int anio )
    {
        for( Maquina maquina : this.maquinas ){
            double suma = 0;
            int cont = 0;
            
            for( int i=0; i < maquina.getListaPedidos().tamanio(); i++ ) {
                Pedido pedido = maquina.getListaPedidos().obtenerPedido(i);
                int mesPedido = Integer.parseInt( pedido.getFechaEntrega().substring(3, 5) );
                int anioPedido = Integer.parseInt( "20" + pedido.getFechaEntrega().substring(6) );
                
                if ( ( mesPedido >= mes && anioPedido == anio ) || anioPedido > anio ) {
                    if( pedido.getEstadoProducto().equals("TRM") ){
                        cont++;
                        suma += pedido.getTiempoFabricacion() / pedido.getCantProductos();
                    }
                    
                }
                
            }
            maquina.setEstimacionTiempoElaboracion(suma / cont );
            
        }
        
    }
    
    public void secuenciarPedidos( String fechaInicio, String horaInicio ) {
        
        
        for( Maquina m : this.maquinas ){
            int i = 0;
            int cont = 0;
            
            while( i < m.getListaPedidos().tamanio() ) {
                Pedido p = m.getListaPedidos().obtenerPedido( i );
                i++;
                if( p.getEstadoProducto().equals("PRG") || p.getEstadoProducto().equals("NPR") ) {
                    // si es un pedido programado o no programado se agrega a la lista auxiliar
                    boolean agregado = false;
                    for( int j=0; j < cont; j++) {
                        Pedido aux = m.getPedidosSecuenciados().obtenerPedido(j);
                        if( comparaFechas( p.getFechaEntrega(), aux.getFechaEntrega()) >= 0 ){
                            m.getPedidosSecuenciados().agregarPedido(p, j);
                            agregado = true;
                            break;
                        }
                    }
                    if( cont == 0 ){
                        m.getPedidosSecuenciados().agregarPedido( p );
                        agregado = true;
                    }
                    if ( !agregado ) {
                        m.getPedidosSecuenciados().agregarPedido( p );
                    }
                    cont++;
                    
                }
                
            }
            
            i = 0;
            cont = 0;
            
            for( int j=0; j < m.getPedidosSecuenciados().tamanio();j++) {
                Pedido p = m.getPedidosSecuenciados().obtenerPedido(j);
                System.out.println( p.getCodMaquina() + " : " + p.getFechaEntrega() );
            }
            
        }
        
    }
    
    // devuelve -1 si f2 es menor que f1, 1 si f2 es mayor que f1 y 0 si son iguales
    public int comparaFechas( String f1, String f2 ){
        if( f1.equals(f2) ){
            return 0;
        }
        
        int anio1 = Integer.parseInt( f1.substring(6) );
        int anio2 = Integer.parseInt( f1.substring(6) );
        if( anio1 < anio2 ) {
            return 1;
        } else if( anio1 > anio2 ) {
            return -1;        
        }
        
        int mes1 = Integer.parseInt( f1.substring(3, 5) );
        int mes2 = Integer.parseInt( f2.substring(3, 5) );
        if( mes1 < mes2 ){
            return 1;
        } else if ( mes1 > mes2 ){
            return -1;
        }
        
        int dia1 = Integer.parseInt( f1.substring(0, 2) );
        int dia2 = Integer.parseInt( f2.substring(0, 2) );
        if( dia1 < dia2 ) {
            return 1;
        }else{
            return -1;
        }
    }
     
}