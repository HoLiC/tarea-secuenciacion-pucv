package proyectoarquitectura;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import jxl.Sheet;
import jxl.Workbook;

public class ProyectoArquitectura {
    
    public static void main(String[] args) {
        
        String fechaInicio = null;
        String horaInicio = null;
        int mes = 0, anio = 0;
        
        //  open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 
        //  read the username from the command-line; need to use try/catch with the
        //  readLine() method
        try {
            System.out.println("Ingrese fecha de inicio:");
            fechaInicio = br.readLine();
            System.out.println("Ingrese hora de inicio:");
            horaInicio = br.readLine();
            System.out.println("Ingrese mes de historia productiva:");
            System.out.println("Considere los meses según número [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]");
            mes = Integer.parseInt( br.readLine() );
            System.out.println("Ingrese año de historia productiva (número):");
            System.out.println("Considere el año en el siguiente formato: 2009, 2010, 2011...");
            anio = Integer.parseInt( br.readLine() );
            
        } catch (IOException ioe) {
            System.out.println("IO error trying to read your name!");
            System.exit(1);
        }
        
        SistemaSecuenciacion ss = new SistemaSecuenciacion( mes, anio );
        ss.calcularTiempoFabricacion(mes, anio);
        
        System.out.println( ss.maquinas[0].getEstimacionTiempoElaboracion() );
        System.out.println( ss.maquinas[1].getEstimacionTiempoElaboracion() );
        System.out.println( ss.maquinas[2].getEstimacionTiempoElaboracion() );
        
        ss.secuenciarPedidos(fechaInicio, horaInicio);
        System.out.println( "Saliendo...");
        System.exit(0);
        
    }
    
}
