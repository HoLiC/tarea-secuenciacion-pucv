
package proyectoarquitectura;

public class Pedido {
    private int numeroPedido;
    private Cliente cliente;
    private String descripcion;
    private int cantProductos;
    private double precio;
    private String fechaEntrega;
    private String codMaquina;
    private String estadoProducto; // "NPR", "PRG", "PRC", "TRM".
    private Double tiempoFabricacion;
    private String fechaTermino;
    
    public Pedido(int np, Cliente cl, String desc, int cant, double pre, String fe, String cod, String est, Double time)
    {
        numeroPedido = np;
        cliente = cl;
        descripcion = desc;
        cantProductos = cant;
        precio = pre;
        fechaEntrega = fe;
        codMaquina = cod;
        estadoProducto = est;
        tiempoFabricacion = time;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public int getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(int numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantProductos() {
        return cantProductos;
    }

    public void setCantProductos(int cantProductos) {
        this.cantProductos = cantProductos;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getCodMaquina() {
        return codMaquina;
    }

    public void setCodMaquina(String codMaquina) {
        this.codMaquina = codMaquina;
    }
    
    public String getEstadoProducto() {
        return estadoProducto;
    }

    public void setEstadoProducto(String estadoProducto) {
        this.estadoProducto = estadoProducto;
    }

    public Double getTiempoFabricacion() {
        return tiempoFabricacion;
    }

    public void setTiempoFabricacion(Double tiempoFabricacion) {
        this.tiempoFabricacion = tiempoFabricacion;
    }

    /**
     * @return the fechaTermino
     */
    public String getFechaTermino() {
        return fechaTermino;
    }

    /**
     * @param fechaTermino the fechaTermino to set
     */
    public void setFechaTermino(String fechaTermino) {
        this.fechaTermino = fechaTermino;
    }
     
}


