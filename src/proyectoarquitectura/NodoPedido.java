
package proyectoarquitectura;

public class NodoPedido {
    private Pedido dato;
    private NodoPedido next;
    
    public NodoPedido(Pedido dato){
        this.dato= dato;
        this.next= null;
    }
    public Pedido getDato(){
        return dato;
    }
    public void setDato(Pedido dato){
        this.dato = dato;
    }

    public NodoPedido getNext() {
        return next;
    }

    public void setNext(NodoPedido next) {
        this.next = next;
    }
    
    
    
}

            
            

