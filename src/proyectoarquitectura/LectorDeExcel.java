/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoarquitectura;

import java.io.File;
import jxl.Sheet;
import jxl.Workbook;

public abstract class LectorDeExcel {
    
    protected ListaPedidos listaPedidos;
    
    
    protected void leerExcel( String codMaquina ) {
        /* se cargan los datos del excel */
        try {
            File xls = new File("Plastibag.xls");
            Workbook libro = Workbook.getWorkbook( xls );
            Sheet hoja1 = libro.getSheet(0);
            
            /* i = numero de columna, j = numero de fila */
            for( int j=1; j < hoja1.getRows(); j++) {
                if( hoja1.getCell(7, j).getContents().equals( codMaquina ) ) {
                    /* valores almacenados */
                    int np = Integer.parseInt( hoja1.getCell(0, j).getContents() );
                    
                    Cliente cl = new Cliente();
                    cl.setCodigoCliente( Integer.parseInt( hoja1.getCell(1, j).getContents() ) );
                    cl.setRazonSocial( hoja1.getCell(2, j).getContents() );
                    
                    String desc = hoja1.getCell(3, j).getContents();
                    int cant = Integer.parseInt( hoja1.getCell(4, j).getContents() );
                    Double pre = Double.parseDouble( hoja1.getCell(5, j).getContents() );
                    String fe = hoja1.getCell(6, j).getContents();
                    
                    String cod = hoja1.getCell(7, j).getContents();
                    String est = hoja1.getCell(8, j).getContents();
                    
                    Double time = null;
                    
                    if( est.equals("TRM") ) {
                        String tiempo = hoja1.getCell(9, j).getContents().replace(",", ".");
                        time = Double.parseDouble( tiempo );
                        
                    }
                    
                    Pedido pedido = new Pedido(np, cl, desc, cant, pre, fe, cod, est, time);
                    listaPedidos.agregarPedido(pedido);
                    
                }
                
            }
            
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
